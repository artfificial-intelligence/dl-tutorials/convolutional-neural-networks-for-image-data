# 영상 데이터를 위한 컨볼루션 신경망 <sup>[1](#footnote_1)</sup>

> <font size="3">컨벌루션 신경망이 이미지 데이터 처리에 어떻게 사용되는지 알아본다.</font>

## 목차

1. [개요](./convolutional-neural-networks-for-image-data.md#intro)
1. [컨볼루션 신경망이란?](./convolutional-neural-networks-for-image-data.md#sec_02)
1. [컨볼루션 신경망의 작동 방식](./convolutional-neural-networks-for-image-data.md#sec_03)
1. [영상 데이터를 위한 컨볼루션 신경망의 응용](./convolutional-neural-networks-for-image-data.md#sec_04)
1. [영상 데이터를 위한 컨볼루션 신경망의 과제와 한계](./convolutional-neural-networks-for-image-data.md#sec_05)
1. [요약](./convolutional-neural-networks-for-image-data.md#summary)

<a name="footnote_1">1</a>: [DL Tutorial 5 — Convolutional Neural Networks for Image Data](https://ai.gopubby.com/dl-tutorial-5-convolutional-neural-networks-for-image-data-8582553f3ddc?sk=fac66d4d92250cccdbcab92f3e588154)를 편역하였다.
